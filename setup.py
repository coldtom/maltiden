#!/usr/bin/env python3

from setuptools import setup

setup(
    name='Maltiden',
    version='0.0.0',
    description='A meal planning suite',
    author='Thomas Coldrick',
    author_email='othko97@gmail.com',
    license='GPLv3',
    url='https://othko.xyz',
    packages=['maltiden'],
    setup_requires=['pytest-runner'],
    tests_require=['pytest']
)
