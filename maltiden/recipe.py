# recipe.py
#
# Copyright 2019 Thomas Coldrick
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from .ingredient import Ingredient

class Recipe():
    """
    Class for a single recipe
    """

    def __init__(self, name):
        """
        Constructor for a recipe. Only a unique name is required.

        Args:
        - name (str) - Unique name for a recipe
        """
        self.name = name

        # Initialise empty list of ingredients
        self._ingredients = []

    # Handle ingredients

    def get_ingredients(self):
        """
        Get the list of ingredients for the recipe.
        """
        return self._ingredients

    def set_ingredients(self, ingredients):
        """
        Set the list of ingredients for a recipe, all at once.

        Args:
        - ingredients (list of Ingredient) - List of ingredients
        """
        self._ingredients = ingredients

    def add_ingredient(self, ingredient):
        """
        Add an ingredient to the ingredients of the recipe

        Args:
        - ingredient (Ingredient) - The ingredient to add
        """
        self._ingredients.append(ingredient)
