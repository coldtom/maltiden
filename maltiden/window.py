# window.py
#
# Copyright 2019 Thomas Coldrick
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from gi.repository import Gtk

from .widgets.headerbar import HeaderBar
from .widgets.mealplanview import MealPlanView

class MaltidenWindow(Gtk.ApplicationWindow):
    __gtype_name__ = 'MaltidenWindow'

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self.setup_view()


    def setup_view(self):
        self._headerbar = HeaderBar()
        self.set_titlebar(self._headerbar)
        self._headerbar.show()

        self._view = MealPlanView()
        self.add(self._view)

        self.show()
