# widgets/mealplanview.py
#
# Copyright 2019 Thomas Coldrick
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from gi.repository import Gtk

from maltiden._utils import Week, Meals

from maltiden.widgets.mealelement import MealElement

MARGIN_SIZE = 25

class MealPlanView(Gtk.Grid):

    __gtype_name__ = "MealPlanView"

    def __init__(self):
        super().__init__()

        self.set_column_spacing(50)
        self.set_row_spacing(50)

        self.set_margin_start(MARGIN_SIZE)
        self.set_margin_end(MARGIN_SIZE)

        self._titles = {day: Gtk.Label(justify=Gtk.Justification.LEFT)
                        for day in Week}
        self._elements = {day: {meal: MealElement()
                                for meal in Meals}
                          for day in Week}

        self._generate_view()

        self.show_all()

    def _generate_view(self):
        for day in Week:
            self._titles[day].set_text(day.name)
            self.attach(self._titles[day], 0, day.value, 1, 1)

            for meal in Meals:
                self._elements[day][meal].set_text(meal.name)
                self.attach(self._elements[day][meal],
                            meal.value, day.value, 1, 1)
