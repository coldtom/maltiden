# widgets/mealelement.py
#
# Copyright 2019 Thomas Coldrick
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from gi.repository import Gtk

@Gtk.Template(resource_path="/xyz/othko/Maltiden/ui/MealElement.ui")
class MealElement(Gtk.Box):

    __gtype_name__ = "MealElement"

    _title = Gtk.Template.Child()
    _menu = Gtk.Template.Child()
    _edit_button = Gtk.Template.Child()
    _delete_button = Gtk.Template.Child()


    def __init__(self, label="None"):
        super().__init__()

        self._title.props.label = label

    def set_text(self, *args, **kwargs):
       self._title.set_text(*args, **kwargs)
