# mealplan.py
#
# Copyright 2019 Thomas Coldrick
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from .recipe import Recipe
from ._utils import Week, Meals

class MealPlan():
    """
    Class for a weekly meal plan.
    """

    def __init__(self):
        self.plan = {day: {meal: None for meal in Meals} for day in Week}

    def set_meal(self, day, meal, recipe):
        """
        Sets the meal `meal` in day `day` to recipe `recipe`.

        Args:
        - day (week.DAY) - Day to set
        - meal (meals.MEAL) - Meal to set
        - recipe (Recipe) - Recipe to set
        """
        self.plan[day][meal] = recipe

    def delete_meal(self, day, meal):
        """
        Remove the meal `meal` in day `day`.

        Args:
        - day (week.DAY) - Day of meal to remove
        - meal (meal.MEAL) - Meal to remove
        """
        self.plan[day][meal] = None
