# ingredient.py
#
# Copyright 2019 Thomas Coldrick
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Ingredient():
    """Class for an ingredient"""

    def __init__(self, name):
        """
        Constructor for an ingredient. The only required argument is a unique
        name for the ingredient.

        Args:
        - name (str) - Unique name of the ingredient
        """

        self.name = name
