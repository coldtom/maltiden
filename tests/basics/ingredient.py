from maltiden.ingredient import Ingredient

def test_ingredient():
    foo = Ingredient('foo')

    assert foo.name == 'foo'
