from maltiden.recipe import Recipe
from maltiden.ingredient import Ingredient

foo = Ingredient('foo')
bar = Ingredient('bar')

def test_recipe():
    recipe = Recipe('test recipe')
    assert recipe.get_ingredients() == []

    recipe.set_ingredients([foo])
    assert recipe.get_ingredients() == [foo]

    recipe.add_ingredient(bar)
    assert recipe.get_ingredients() == [foo, bar]
    
