from maltiden.recipe import Recipe
from maltiden.mealplan import MealPlan
from maltiden._utils import Week, Meals

foo = Recipe('foo')
bar = Recipe('bar')

def test_mealplan():
    mealplan = MealPlan()

    mealplan.set_meal(Week.MONDAY, Meals.BREAKFAST, foo)
    mealplan.set_meal(Week.THURSDAY, Meals.DINNER, bar)

    expected = {day: {meal: None for meal in Meals} for day in Week}
    expected[Week.MONDAY][Meals.BREAKFAST] = foo
    expected[Week.THURSDAY][Meals.DINNER] = bar

    assert mealplan.plan == expected

    mealplan.delete_meal(Week.MONDAY, Meals.BREAKFAST)

    expected[Week.MONDAY][Meals.BREAKFAST] = None

    assert mealplan.plan == expected
